package com.weffle.socket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    private static final String ADDRESS = "127.0.0.1";
    private static final int PORT = 420;

    public static void send(String s) {
        try (Socket socket = new Socket(InetAddress.getByName(ADDRESS), PORT)) {
            DataOutputStream output =
                    new DataOutputStream(socket.getOutputStream());
            output.writeUTF(s);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
