package com.weffle.socket;

import com.weffle.Task;
import com.weffle.WebApplication;
import org.jdeferred2.Deferred;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static Server server = new Server();
    private static final int PORT = 1488;
    private Socket socket;

    private Server() {
    }

    public static void start() throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Server.server.socket = server.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, "Server thread").start();
    }

    public static void submitDonation(JSONObject donation, Deferred deferred) {
        Task.put(donation.getString("task_id"), deferred);
        sendDonation(donation);
    }

    private static void sendDonation(JSONObject donation) {
        if (server.socket.isClosed())
            return;
        try {
            DataOutputStream output =
                    new DataOutputStream(server.socket.getOutputStream());
            output.writeUTF(donation.toString());
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
