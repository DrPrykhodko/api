package com.weffle.resources;

import com.weffle.WebApplication;
import com.weffle.socket.Client;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

@Path("twitch")
public class Twitch {
    @Path("stream")
    @GET
    public Response stream(@QueryParam("hub.challenge") String challenge,
                           String s) {
        return Response.ok().entity(challenge).build();
    }

    @Path("stream")
    @POST
    public Response stream(@Context HttpServletRequest request,
                           String s) {
        String signature = request.getHeader("X-Hub-Signature");
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(
                    WebApplication.CLIENT_SECRET.getBytes("UTF-8"),
                    "HmacSHA256");
            mac.init(secret_key);

            String hash = "sha256=" + DatatypeConverter.printHexBinary(
                    mac.doFinal(s.getBytes("UTF-8"))).toLowerCase();
            if (hash.equals(signature))
                Client.send(s);
            else
                throw new IllegalArgumentException();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }
}