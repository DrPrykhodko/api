package com.weffle.resources;

import com.weffle.Task;
import com.weffle.WebApplication;
import com.weffle.socket.Server;
import org.jdeferred2.Deferred;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.atomic.AtomicReference;

@Path("cuckmod")
public class Cuckmod {
    @POST
    @Path("submit")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf8")
    public Response submit(String donationJson) {
        JSONObject donation = new JSONObject(donationJson);
        donation.put("task_id", Task.getFreshId());
        Deferred deferred = new DeferredObject();
        Promise promise = deferred.promise();
        AtomicReference<String> result = new AtomicReference<>();
        promise.done(stringOneResult -> result.set((String) stringOneResult));
        Server.submitDonation(donation, deferred);
        try {
            promise.waitSafely(60000);
            return Response.ok().entity(result.get()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @POST
    @Path("accept")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf8")
    public Response accept(String donationJson) {
        JSONObject donation = new JSONObject(donationJson);
        String taskId = donation.getString("task_id");
        Deferred deferred = Task.get(taskId);
        if (deferred == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        deferred.resolve(donation.toString());
        Task.remove(taskId);
        return Response.ok().build();
    }
}
