package com.weffle;

import org.jdeferred2.Deferred;

import java.util.concurrent.ConcurrentHashMap;

public class Task {
    private static Task task = new Task();
    private static ConcurrentHashMap<String, Deferred> tasks;
    private static int lastTaskId;

    private Task() {
        tasks = new ConcurrentHashMap<>();
    }

    public static void put(String taskId, Deferred deferred) {
        tasks.put(taskId, deferred);
    }

    public static Deferred get(String taskId) {
        return tasks.get(taskId);
    }

    public static Deferred remove(String taskId) {
        return tasks.remove(taskId);
    }

    public synchronized static String getFreshId() {
        return Integer.toString(lastTaskId++);
    }
}
