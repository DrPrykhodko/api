package com.weffle;

import com.weffle.socket.Server;
import org.jdeferred2.Deferred;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@ApplicationPath("/")
public class WebApplication extends Application {
    public static final String CLIENT_SECRET = "8dz2m82n6lxwc2pv2i2hpmv09xirhi";

    public WebApplication() throws IOException {
        super();
        Server.start();
    }
}
